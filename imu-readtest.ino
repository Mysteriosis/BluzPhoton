#include "SparkFunLSM9DS1/SparkFunLSM9DS1.h"

//////////////////////////
// LSM9DS1 Library Init //
//////////////////////////
// Use the LSM9DS1 class to create an object. [imu] can be
// named anything, we'll refer to that throught the sketch.
LSM9DS1 imu;

///////////////////////
// Example I2C Setup //
///////////////////////
// SDO_XM and SDO_G are both pulled high, so our addresses are:
#define LSM9DS1_M   0x1E // Would be 0x1C if SDO_M is LOW
#define LSM9DS1_AG  0x6B // Would be 0x6A if SDO_AG is LOW

////////////////////////////
// Sketch Output Settings //
////////////////////////////
#define PRINT_SPEED 3000 // 250 ms between prints

//Seulement en firmware 0.61
//LEDStatus blinkRed(RGB_COLOR_RED, LED_PATTERN_BLINK);
String data;
bool ready = true;

void setup() 
{
    // Before initializing the IMU, there are a few settings
    // we may need to adjust. Use the settings struct to set
    // the device's communication mode and addresses:
    imu.settings.device.commInterface = IMU_MODE_I2C;
    imu.settings.device.mAddress = LSM9DS1_M;
    imu.settings.device.agAddress = LSM9DS1_AG;
    // The above lines will only take effect AFTER calling
    // imu.begin(), which verifies communication with the IMU
    // and turns it on.
    if (!imu.begin())
    {
        data = String("Failed to communicate with LSM9DS1.");
        Particle.publish("Error", data);
        
        RGB.control(true);
        RGB.color(255, 0, 0);
        ready = false;
        
        //Seulement en firmware 0.61
        //blinkRed.setActive(true);
    }
}

void loop()
{
    if(ready) {
        imu.readGyro();
        data = String("{x:" + String(imu.calcGyro(imu.gx)) + ",y:" + String(imu.calcGyro(imu.gy)) + ",z:" + String(imu.calcGyro(imu.gz)) + "}");
        Particle.publish("IMU_Gyro", data);
        
        imu.readAccel();
        data = String("{x:" + String(imu.calcGyro(imu.ax)) + ",y:" + String(imu.calcGyro(imu.ay)) + ",z:" + String(imu.calcGyro(imu.az)) + "}");
        Particle.publish("IMU_Accel", data);
        
        imu.readMag();
        data = String("{x:" + String(imu.calcGyro(imu.mx)) + ",y:" + String(imu.calcGyro(imu.my)) + ",z:" + String(imu.calcGyro(imu.mz)) + "}");
        Particle.publish("IMU_Mag", data);

        delay(PRINT_SPEED);
    }
}