class LedControl {
  public:
    LedControl() {
        pinMode(D7, OUTPUT);
        Particle.variable("ledStatus", this->ledStatus);
        Particle.function("toggleLED", &LedControl::toggleLED, this);
    }

    int toggleLED(String command) {
        if(command=="on") {
            if(this->ledStatus == 0) {
                digitalWrite(D7, HIGH);
                this->ledStatus = 1;
            }
        }
        else if(command=="off") {
            if(this->ledStatus == 1) {
                digitalWrite(D7, LOW);
                this->ledStatus = 0;
            }
        }
        else {
            return -1;
        }
        
        return 0;
    }
    
  private:
    int ledStatus = 0;
};

LedControl ledController;